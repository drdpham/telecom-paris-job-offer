= AsciiDoc Project Development
:document-type: Master Internship Offer
:author: John Smith
:author-function: Supervisor
:author-email: jdoe@example.com
:author-phone: +33 1 23 45 67 89
:organization: Télécom Paris
:doc-id: abcd-1234
:location: Paris (France)
:current-date: 2024-02-01
:duration: 6 months
:degree: Master
:fields: Computer Science, Electrical Engineering
:date-start: 2024-03-01
:logo-image-left: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/telecomparis_endossem_ipp_rvb_300pix.png[pdfwidth=3cm]
:logo-image-right: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/IMT-PNG.png[pdfwidth=3cm]
:logo-image-footer: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/barette-tri-telecom-paris.svg[pdfwidth=3.6cm]
:footer-text-l1: 19 Place Marguerite Perey - 91120 Palaiseau - France • Tél. +33 (0)1 75 31 92 01 • Siret : 180 092 025 00162 • Code APE : 854Z  – Enseignement supérieur
:footer-text-l2: link:www.telecom-paris.fr[]
:footer-text-l3: 
:lang: en
:stem: latexmath
ifdef::backend-pdf[]
:pdf-theme: telecomparis
:imagesoutdir: prod/images_pdf_renderer
endif::[]
ifdef::backend-html5[]
:toc: left
:imagesoutdir: images_html_renderer
// https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes-ref/#document-metadata-attributes
endif::[]
ifdef::backend-docbook5[]
:docdate: {current-date}
endif::[]

ifdef::backend-html5,backend-docbook5[]
[.subtitle]
{document-type}


[role=mastinfo,cols="2,>1",grid=none,frame=none,subs="attributes+", separator="|"]
|===
|{author-function} : {author}                | {location}, {current-date}
|Email             : mailto:{author-email}[] | Ref. : {doc-id}
|Phone             : {author-phone}          | 
|Duration          : {duration}              | 
|Degree            : {degree}                | 
|Fields            : {fields}                | 
|Start date        : {date-start}            | 
|===
endif::[]

== Context

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

== Subject positioning and objective

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

[stem]
++++
\lim_{n \to \infty}\frac{n}{\sqrt[n]{n!}} = {\mathrm e}
++++

== Work Plan (6 months)

// https://docs.asciidoctor.org/asciidoc/latest/subs/apply-subs-to-blocks/#incremental
ifdef::backend-pdf[]
[mermaid,pdfwidth=100%,subs="attributes+",format="svg"]
endif::[]
ifdef::backend-html5[]
[mermaid,subs="attributes+",format="svg",svg-type="inline"]
endif::[]
ifdef::backend-docbook5[]
[mermaid,subs="attributes+"]
endif::[]
....
gantt
    title {project-title}
    dateFormat YYYY-MM-DD
    section Section
        A task          :a1, {date-start}, 30d
        Another task    :after a1, 20d
    section Another
        Task in Another :2024-04-01, 12d
        another task    :24d
....

== Required skills

* Skill 1
* Skill 2
* Skill 3

== Extra text

At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.

_The Pragmatic Programmer_ <<pp>> should be required reading for all developers.
To learn all about design patterns, refer to the book by the "`Gang of Four`" <<gof>>.

[bibliography]
== References

* [[[pp]]] Andy Hunt & Dave Thomas. The Pragmatic Programmer:
From Journeyman to Master. Addison-Wesley. 1999.
* [[[gof,gang]]] Erich Gamma, Richard Helm, Ralph Johnson & John Vlissides.
Design Patterns: Elements of Reusable Object-Oriented Software. Addison-Wesley. 1994.