source      := Internship-2024-ProjABCD.adoc

target_ext 	:= pdf html docx
dest_base 	:= prod

$(foreach ext,$(target_ext),$(eval dest_$(ext) := $(dest_base)/$(ext)))
# provides dest_pdf and dest_html
$(foreach ext,$(target_ext),$(eval target_$(ext) := $(basename $(source)).$(ext)))
# provides target_pdf and target_html
$(foreach ext,$(target_ext),$(eval target_$(ext)_full_path := $(dest_$(ext))/$(target_$(ext))))
# provides target_pdf_full_path and target_html_full_path

theme_files := $(wildcard pdf-theme/*) $(wildcard css/*)

targets := $(target_pdf_full_path) $(target_html_full_path) $(target_docx_full_path)
$(info targets: $(targets))

ASCIIDOCTOR_PDF := $(HOME)/.ruby_gems_perso/bin/asciidoctor-pdf

PDF_THEME_DIR           = ./pdf-theme
CUSTOM_CSS_DIR          = ./css
CUSTOM_CSS              = $(CUSTOM_CSS_DIR)/asciidoc.css
DOCBOOK_CONVERTER_DIR   = ./docbook
DOCBOOK_CONVRTR         = $(DOCBOOK_CONVERTER_DIR)/docbook-patch-biblio.rb

# asciidoctor-diagram enables the use of diagrams
# asciidoctor-mathematical creates images for math - do not use it here ; uses MathJax
ADOC_OPT := -r asciidoctor-diagram \
			-a allow-uri-read \
			-a stylesheet=$(CUSTOM_CSS)

# https://docs.asciidoctor.org/pdf-converter/latest/stem/#mathematical-format
# https://docs.asciidoctor.org/asciidoctor/latest/stem/mathematical/
# asciidoctor-mathematical creates images for math
ADOC_PDF_OPT := -r asciidoctor-mathematical -a mathematical-format=svg \
				-r $(PDF_THEME_DIR)/telecomparis-pdf-converter.rb \
				-a pdf-themesdir=$(PDF_THEME_DIR) \
				$(ADOC_OPT)

all: $(targets) 

# out.o: src.c src.h
#   $@   # "out.o" (target)
#   $<   # "src.c" (first prerequisite)
#   $^   # "src.c src.h" (all prerequisites)

# %.o: %.c
#   $*   # the 'stem' with which an implicit rule matches ("foo" in "foo.c")

# also:
#   $+   # prerequisites (all, with duplication)
#   $?   # prerequisites (new ones)
#   $|   # prerequisites (order-only?)

#   $(@D) # target directory

$(target_pdf_full_path): $(source) $(theme_files)
	$(ASCIIDOCTOR_PDF) -D $(@D) $(ADOC_PDF_OPT) $<

$(target_html_full_path): $(source) $(theme_files)
	asciidoctor -o $@ $(ADOC_OPT) $<
	mkdir -p $(@D)/images_html_renderer_useless
	mv -f images_html_renderer/* $(@D)/images_html_renderer_useless/
	rmdir images_html_renderer


# pandoc searches in the current directory for the images of the docbook file
# The images are in prod/docx/
# The docbook file is in prod/docx/ 
# So we need to cd to prod/docx/ before running pandoc
$(target_docx_full_path): $(source) $(theme_files)
	asciidoctor -d article -b docbook5 -r asciidoctor-diagram -r $(DOCBOOK_CONVRTR) --trace -o $(basename $@).docbook $<
	cd $(@D) && \
	pandoc -f docbook -t docx -o $(notdir $@) $(basename $(notdir $@)).docbook

clean:
	rm -rf prod

.PHONY: all clean