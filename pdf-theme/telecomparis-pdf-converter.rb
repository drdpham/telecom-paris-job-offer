# = Master Internship Project Offer
# :project-title: AsciiDoc Project Development
# :author: John Smith
# :author-function: Supervisor
# :author-email: jdoe@example.com
# :author-phone: +33 1 23 45 67 89
# :organization: Télécom Paris
# :doc-id: abcd-1234
# :location: Paris, France
# :current-date: 2024-02-01
# :duration: 6 months
# :degree: Master
# :fields: Computer Science, Electrical Engineering
# :date-start: 2024-03-01
# :logo-image-left: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/telecomparis_endossem_ipp_rvb_300pix.png[pdfwidth=3cm]
# :logo-image-right: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/IMT-PNG.png[pdfwidth=3cm]
# :logo-image-footer: image:https://seminaires-c2s.telecom-paristech.fr/logos_tp_ipp/barette-tri-telecom-paris.svg[pdfwidth=3.6cm]
# :footer-text-l1: 19 Place Marguerite Perey - 91120 Palaiseau - France • Tél. +33 (0)1 75 31 92 01 • Siret : 180 092 025 00162 • Code APE : 854Z  – Enseignement supérieur
# :footer-text-l2: link:www.telecom-paris.fr[]
# :footer-text-l3: 
# :lang: en
# ifdef::backend-pdf[]
# :pdf-theme: telecomparis
# :pdf-themesdir: pdf-theme
# endif::[]

require 'date'

# https://docs.asciidoctor.org/pdf-converter/latest/extend/use-cases/#custom-article-title-with-details
class PDFConverterArticleTitleWithAuthorAndDate < (Asciidoctor::Converter.for 'pdf')
    register_for 'pdf'
  
    def ink_general_heading doc, title, opts
      return super unless opts[:role] == :doctitle
      doctypetitle = (doc.attr 'document-type')
      ink_document_title doctypetitle, opts
      typeset_text_opts = {text_transform: 'none'}.merge opts
      ink_document_title title, typeset_text_opts
      ink_document_details doc, opts
      margin_bottom @theme[:heading_h1_margin_bottom] || @theme.heading_margin_bottom
    end
  
    def ink_document_title title, opts
      if (top_margin = @theme.heading_h1_margin_page_top || @theme.heading_margin_page_top) > 0
        move_down top_margin
      end
      pad_box @theme.heading_h1_padding do
        if (transform = resolve_text_transform opts)
          title = transform_text title, transform
        end
        if (inherited = apply_text_decoration font_styles, :heading, 1).empty?
          inline_format_opts = true
        else
          inline_format_opts = [{ inherited: inherited }]
        end
        typeset_text_opts = { color: @font_color, inline_format: inline_format_opts }.merge opts
        typeset_text title, (calc_line_metrics (opts.delete :line_height) || @base_line_height), typeset_text_opts
      end
    end

    # Define a function ink_document_details to display: author, author-function, author-email, author-phone, organization, doc-id, location, current-date, duration, degree, fields, date-start, lang,
    def ink_document_details doc, opts
        author = doc.attr 'author'
        author_function = doc.attr 'author-function'
        author_email = doc.attr 'author-email'
        author_phone = doc.attr 'author-phone'
        organization = doc.attr 'organization'
        doc_id = doc.attr 'doc-id'
        location = doc.attr 'location'
        current_date_str = doc.attr 'current-date'
        current_date = current_date_str ? Date.parse(current_date_str).strftime('%B %d, %Y') : nil
        duration = doc.attr 'duration'
        degree = doc.attr 'degree'
        fields = doc.attr 'fields'
        date_start = doc.attr 'date-start'
        lang = doc.attr 'lang'
        if lang == 'fr'
            email_label = 'Courriel'
            phone_label = 'Tél'
            duration_label = 'Durée'
            degree_label = 'Diplôme'
            fields_label = 'Domaines'
            date_start_label = 'Date de début'
            doc_id_label = 'Réf.'
            author_function_default = 'Encadrant de stage'
            current_date_label = 'le '
        else # lang == 'en'
            email_label = 'Email'
            phone_label = 'Phone'
            duration_label = 'Duration'
            degree_label = 'Degree'
            fields_label = 'Fields'
            date_start_label = 'Start date'
            doc_id_label = 'Ref.'
            author_function_default = 'Internship Supervisor'
            current_date_label = ''
        end

        if author_function.nil? || author_function.empty?
            author_function = author_function_default
        end

        if (vskip = @theme.heading_h1_details_margin_top) > 0
            move_down vskip
        end
        
        float do
            if location || current_date
                location_date_separator = location && current_date ? %(, ) : ''
                theme_font_cascade [:base, :heading_h1_details] do
                    # ink_prose %(#{location}), margin: 0, align: :right
                    ink_prose %(#{location}#{location_date_separator}#{current_date_label}#{current_date}), margin: 0, align: :right
                end
            end
            # if current_date
            #     theme_font_cascade [:base, :heading_h1_details] do
            #         ink_prose %(#{current_date_label} : #{current_date}), margin: 0, align: :right
            #     end
            # end
            if doc_id
                theme_font_cascade [:base, :heading_h1_details] do
                    ink_prose %(#{doc_id_label} : #{doc_id}), margin: 0, align: :right
                end
            end
        end


        if author
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{author_function} : #{author}), margin: 0
            end
        end
        if author_email
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{email_label} : <a href="mailto:#{author_email}">#{author_email}</a>), margin: 0
            end
        end
        if author_phone
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{phone_label} : #{author_phone}), margin: 0
            end
        end
        if duration
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{duration_label} : #{duration}), margin: 0
            end
        end
        if degree
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{degree_label} : #{degree}), margin: 0
            end
        end
        if fields
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{fields_label} : #{fields}), margin: 0
            end
        end
        if date_start
            theme_font_cascade [:base, :heading_h1_details] do
                ink_prose %(#{date_start_label} : #{date_start}), margin: 0
            end
        end
        
    end

  end