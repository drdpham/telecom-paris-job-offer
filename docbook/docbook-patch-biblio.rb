# The functions to be patched have been found by analyzing the docbook converter source code: 
# https://github.com/asciidoctor/asciidoctor/blob/main/lib/asciidoctor/converter/docbook5.rb
# based on the tags to be removed, known from previous tests: 
# https://gitlab.com/drdpham/simple-project-presentation/-/blob/master/tools/removeDocBookbibliodiv.py
#
# in convert_ulist, the bibliodiv and bibliomixed tags are removed
# in convert_inline_anchor, the bibref section is modified to add the <xref linkend=> tag
#                           and the text is modified to remove the reference string from the original text


class Docbook5BiblioPatch < (Asciidoctor::Converter.for 'docbook5')
register_for 'docbook5'

    def convert_ulist node
        result = []
        if node.style == 'bibliography'
            result << %(#{common_attributes node.id, node.role, node.reftext})
            result << %(<title>#{node.title}</title>) if node.title?
            node.items.each do |item|
            result << ''
            result << %(<bibliomisc>#{item.text}</bibliomisc>)
            result << item.content if item.blocks?
            result << ''
            end
            result << ''
        else
            mark_type = (checklist = node.option? 'checklist') ? 'none' : node.style
            mark_attribute = mark_type ? %( mark="#{mark_type}") : ''
            result << %(<itemizedlist#{common_attributes node.id, node.role, node.reftext}#{mark_attribute}>)
            result << %(<title>#{node.title}</title>) if node.title?
            node.items.each do |item|
            text_marker = (item.attr? 'checked') ? '&#10003; ' : '&#10063; ' if checklist && (item.attr? 'checkbox')
            result << %(<listitem#{common_attributes item.id, item.role}>)
            result << %(<simpara>#{text_marker || ''}#{item.text}</simpara>)
            result << item.content if item.blocks?
            result << '</listitem>'
            end
            result << '</itemizedlist>'
        end
        result.join "\n"
    end

    def convert_inline_anchor node
        case node.type
        when :ref
            %(<anchor#{common_attributes((id = node.id), nil, node.reftext || %([#{id}]))}/>)
        when :xref
            if (path = node.attributes['path'])
            %(<link xl:href="#{node.target}">#{node.text || path}</link>)
            else
            if (linkend = node.attributes['refid']).nil_or_empty?
                root_doc = get_root_document node
                # Q: should we warn instead of generating a document ID on demand?
                linkend = (root_doc.id ||= generate_document_id root_doc)
            end
            # NOTE the xref tag in DocBook does not support explicit link text, so the link tag must be used instead
            # The section at http://www.sagehill.net/docbookxsl/CrossRefs.html#IdrefLinks gives an explanation for this choice
            # "link - a cross reference where you supply the text of the reference as the content of the link element."
            (text = node.text) ? %(<link linkend="#{linkend}">#{text}</link>) : %(<xref linkend="#{linkend}"/>)
            end
        when :link
            %(<link xl:href="#{node.target}">#{node.text}</link>)
        when :bibref
            # the variable text contains the text of the anchor, which begins with the reference string with the following pattern "[id-string]"
            # create a new text and remove the reference string from the original text
            new_ref_str = %(#{text})
            new_ref_str = new_ref_str.sub(/\[.*\]/, '')

            %(<anchor#{common_attributes node.id, nil, (text = "[#{node.reftext || node.id}]")}/><xref linkend="#{node.id}"/>#{new_ref_str})
        else
            logger.warn %(unknown anchor type: #{node.type.inspect})
            nil
        end
    end


end